# CICD con Gitlab: creando Pipelines

## ¿Por qué es recomendable este taller?

Lo es por diversos motivos:

1. Hay una tendencia clara a la automatización de pruebas. Dicha automatización, permite ejecutar las pruebas de una manera más rápida que manualmente.
2. El testing continuo es el proceso de ejecutar pruebas automáticas en pipelines. Nos permite obtener un feedback lo antes posible de la última versión antes de ser entregada o desplegada.
3. Una pipeline es una cadena de procesos conectados de tal forma que la salida de uno es la entrada del otro. Posteriormente veremos algún ejemplo. Las pipelines nos permiten automatizar el proceso desde que se realiza un commit hasta que se entrega, despliega, etc…

## Objetivos

Objetivos:

1. Aprender los conceptos de CICD. A saber: integración continua, entrega continua y despliegue continuo.
2. Diferencias entre los conceptos mencionados anteriormente.
3. Aprender qué es una pipeline y el por qué de su utilidad.
4. Crear alguna pipeline en Gitlab.
5. Ejecutar de dichas pipelines de ejemplo.

## Introducción

A pesar de la evolución de los últimos tiempos, se siguen produciendo bugs.

El testing continuo es el proceso que ejecuta de manera continua los tests automáticos para poder detectar de una manera temprana los bugs introducidos por los últimos cambios de código. Cabe destacar, que cuanto antes se detecte un error, menor será el coste de repararlo.

Vamos a ver el concepto de pipeline, que es un conjunto de acciones secuenciales, donde la salida de uno es la entrada del siguiente. Según las pipelines, veremos las estrategias de CICD:

1. CI: integración continua
2. CD: entrega continua.
3. CD: despliegue continuo.

Por último haremos uso de la herramienta de control de versiones Gitlab, que nos permitirá diseñar algunas pipelines en una de sus secciones, y donde pondremos en práctica todos estos conceptos aprendidos.

## CICD

### Conceptos básicos de la CI/CD:

1. La CICD es un método para distribuir aplicaciones a los clientes con frecuencia mediante el uso de la automatización en las etapas del desarrollo de aplicaciones.
2. Es una solución para los problemas que puede generar la integración del código nuevo a los equipos de desarrollo y operaciones.
3. Incorpora la automatización continua y el control permanente en todo el ciclo de vida de las aplicaciones, desde las etapas de integración y prueba hasta las de distribución y despliegue.
   4.Existen distintos tipos:
   4.1. CI: integración continua
   4.2.CD: distribución continua y despliegue continuo.

### ¿Qué es una CI?

1. Es un proceso de automatización para los desarrolladores.
2. Si la CI tiene éxito, los cambios de código se diseñan, se prueban y se combinan periódicamente en un repositorio compartido.
3. Así se evita el problema de que se desarrollen demasiadas divisiones de una aplicación al mismo tiempo, ya que podían entrar en conflicto entre sí.

### ¿Qué es CD (Continuous Delivery)?

1. Después de la automatización de los diseños y las pruebas de unidad e integración de la CI, la distribución continua automatiza la liberación de ese código validado hacia un repositorio.
2. Es importante que la integración continua ya esté incorporada al canal de desarrollo.
3. El objetivo es tener una base de código que pueda desplegarse en producción en cualquier momento.

### ¿Qué es una CD (Continuous Deployment)?

1. Automatiza el lanzamiento de una aplicación a producción. Es una extensión de la entrega continua.
2. Implica que el cambio implementado por un desarrollador en una aplicación pueda ponerse en marcha unos cuantos minutos después de escribirlo, siempre y cuando haya pasado las pruebas automatizadas.

## ¿Qué es una pipeline?

A continuación vamos a ver qué es una pipeline.

1. Es un proceso que automatiza el CICD, es decir, integración, entrega y despliegue.
2. Veremos que tiene una extensión de fichero yml.
3. Está compuesto por stages y cada uno de ellos por jobs o scripts. Por ejemplo:
   3.1. Stages: build, test and deploy.
4. En el siguiente enlace se encuentran numerosas plantillas de [enlace][pipelines]

También debemos conocer las siguientes características acerca de las pipelines:

1. Se pueden usar imágenes de docker, por ejemplo, podemos usar nodejs para ejecutar pruebas de protractor.
2. Cuando hacemos un commit, se detecta dicho cambio y de manera automática se lanza dicha pipeline.

Para ello solo necesitamos tener una cuenta en [Gitlab](https://gitlab.com/users/sign_in)

## Casos prácticos

Veremos algunos casos prácticos:

1. Pipeline básica con bash en la que veremos cómo crear varios stages.

   ```sh
    # 1.1. Para descargarla: 
   git clone https://gitlab.com/AlfredoBazo/opw-bash
   ```

2. Por último haremos uso de una pipeline más compleja en la que ejecutaremos unos tests de postman haciendo uso del paquete newman.

   ```sh
   # 2.1. Para descargar el proyecto:
   git clone https://gitlab.com/AlfredoBazo/cicd-pipeline-poc
   ```