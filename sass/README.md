<h1>SASS</h1>

<h3>Juan Diego</h3>

<h1>Tabla de Contenido</h1>

- [INTRODUCCIÓN](#introducción)
  - [Presentación](#presentación)
  - [Glosario básico](#glosario-básico)
  - [Entorno de trabajo](#entorno-de-trabajo)
  - [Estructura del resto del curso](#estructura-del-resto-del-curso)
- [SASS: UN PREPROCESADOR CSS](#sass-un-preprocesador-css)
  - [Características e instalación](#características-e-instalación)
  - [Ventajas y desventajas de usar Sass](#ventajas-y-desventajas-de-usar-sass)
  - [Elementos básicos de Sass](#elementos-básicos-de-sass)
  - [Compilando archivos Sass](#compilando-archivos-sass)
  - [Ejercicio práctico I](#ejercicio-práctico-i)
  - [Estructuras de control y funciones](#estructuras-de-control-y-funciones)
  - [Ejercicio práctico II](#ejercicio-práctico-ii)
  - [Reglas y directivas](#reglas-y-directivas)
  - [Mixins](#mixins)
  - [Documentando Sass con SassDoc](#documentando-sass-con-sassdoc)
  - [Ejercicio práctico III](#ejercicio-práctico-iii)
- [GULP](#gulp)
  - [Instalación de Gulp](#instalación-de-gulp)
  - [Gulpfile.js y tareas Gulp](#gulpfilejs-y-tareas-gulp)
  - [Plugins Gulp](#plugins-gulp)
  - [Workflow con Gulp](#workflow-con-gulp)
- [CREACIÓN DE MI PROPIO TEMA BOOTSTRAP USANDO SASS](#creación-de-mi-propio-tema-bootstrap-usando-sass)
  - [Descargando BootStrap](#descargando-bootstrap)
  - [Comprendiendo la organización del proyecto](#comprendiendo-la-organización-del-proyecto)
  - [Creando mi tema (Parte I)](#creando-mi-tema-parte-i)
  - [Creando mi tema (Parte II)](#creando-mi-tema-parte-ii)
  - [Generando un nuevo tema con Sass/Gulp](#generando-un-nuevo-tema-con-sassgulp)
- [MI PROPIO FRAMEWORK](#mi-propio-framework)
  - [Estructura de mi proyecto](#estructura-de-mi-proyecto)
  - [Workflow de Sass/Gulp](#workflow-de-sassgulp)
  - [Creando mi framework (Parte I)](#creando-mi-framework-parte-i)
  - [Creando mi framework (Parte II)](#creando-mi-framework-parte-ii)
  - [Creando mi framework (Parte III)](#creando-mi-framework-parte-iii)
  - [Usando mi propio framework](#usando-mi-propio-framework)

# INTRODUCCIÓN

## Presentación

Presentación general del curso en el que se aclararán los siguientes aspectos:

- ¿Por qué este curso?.
- Requisitos para poder cursarlo sin problemas.
- Qué es lo que cubre el curso.
- Qué es lo que no cubre el curso.

[sass | Github](https://github.com/pekechis/OpenWebinarsDesarrollo_FrontEd_con_Sass_Gulp)

## Glosario básico

- Front-End
- Preprocesador CSS
- Web Component
- Build-Tools
- Workflow

## Entorno de trabajo

Presentación de las distintas herramientas que utiliza el autor del curso para crear y desarrollar el mismo.

- Navegador Firefox.
- Visual Studio Code como editor de código.
- Propuesta de extensiones específicas para el editor de código anterior.
- Herramienta de control de versiones Git y GitHub.
- NPM (Node Package Manager) para realizar ciertas instalaciones.
- BootStrap, un FrameWork CSS que modificaremos para crear nuestro propio tema.
- Sass como preprocesador CSS.
- Gulp como Task Runner.

## Estructura del resto del curso

# SASS: UN PREPROCESADOR CSS

## Características e instalación

Es este apartado vamos a describir las características generales de la nueva versión de Sass (dart-sass)

### Ventajas y desventajas de usar Sass

La utilización de Sass tiene todas aquellas **ventajas** derivadas de la utilización de un preprocesador CSS. De manera simplificada podemos decir que Sass me va a permitir el **desarrollo de proyectos CSS complejos** de una manera más **eficiente** y **organizada**.

### Ventajas

Concretando más, podemos enumerar la siguiente ventajas (al menos):

- **Reduce el tiempo** para crear y mantener mis ficheros CSS.
- Permite una **organización modular** de mis estilos. Esto es especialmente beneficioso en proyectos grandes con hojas de estilos muy grandes. Podremos dividir y unir esas hojas de estilos según nos convenga.
- Proporciona **estructuras propias de lenguajes de programación** como *variables, listas, funciones, estructuras de control* etc…
- Me permite crear distintos tipos de hojas de estilos, es decir **distintas salidas**, dependiendo de cuál sea mi objetivo (desarrollo, producción etc..). Podremos generar hojas de estilos comprimidas, minizadas, normales etc..
- La *nueva versión* dart-Sass tiene muchas **menos dependencias** que las versiones anteriores que dependían de todo el entorno de Ruby y su gems.
- Hay una gran comunidad asociada, muchas librerías en base a Sass y muchas herramientas que le dan soporte.

### Desventajas

Pese a todas esas ventajas expuestas anteriormente el uso de un preprocesador presenta también algún inconveniente:

- **Necesidad de aprender** una nueva herramienta. No estoy seguro de que esto sea una desventaja pero lo que sí es cierto es que eso lleva tiempo.
- Se añade un **tiempo de compilación** para obtener nuestros estilos. El proceso no es directo como anteriormente.
- La **complejidad es mayor**. Sass tiene una sintaxis más compleja que CSS.

Estas desventajas son propias de cualquier preprocesador, no solo de Sass.

### Conclusión

No obstante **DEBES USARLO**. Te darás cuenta, conforme tus proyectos CSS/Front-End vayan creciendo, de que una herramienta como un preprocesador CSS es algo imprescindible para ahorrarte tiempo y dolores de cabeza.

### Elementos básicos de Sass

En este apartado vamos a comenzar a practicar con Sass. Para ello introduciremos y usaremos sus elementos básicos.

Sin embargo, antes de lanzarnos a todo esto me parece importante destacar, ya que este curso lleva la palabra ***profesional*** en el título, el hecho de que para usar de manera eficiente Sass en nuestros proyectos Web es muy recomendable establecer ciertas reglas, guías o convenciones que nos sirvan para optimizar nuestro esfuerzo haciendo que el código que escribimos sea ***reusable,escalable y mantenible\***.

Hay multitud de guías por ahí pero en relación a Sass hay una que me gusta especialmente. Os recomiendo que la tengáis siempre a mano.

[Sass Guideline](https://sass-guidelin.es/es/)

Además, por supuesto, también hay que tener siempre presente la documentación oficial:

[Sass - Documentación Oficial](https://sass-lang.com/documentation/)

Una vez aclarado esto, los elementos básicos que vamos a ver en este apartado son:

- Variables
- Comentarios
- Listas y Mapas
- Interpolación
- Anidamiento
- Mi primera compilación

### Variables

Las ***variableS\*** son elementos típicos de los lenguajes de programación y de manera muy básica podemos definirlos como contenedores donde ***guardaremos\*** ciertos ***valores\*** para poder utilizarlos con posterioridad.

**Sass** nos proporciona la posibilidad de trabajar con variables y podemos distinguir:

- La definición de la variable, que es donde le damos nombre y normalmente un valor que puede ser un valor en sí mismo o el resultado de una expresión.
- El uso de esas variables y, por consiguiente, de sus valores asociados.

Un ejemplo:

```scss
//Definición de las variables con sus valores
$cabecera-fondo: rgba(200, 200, 200, 0.8);
$cabecera-anchura: 80%;

//Uso de las variables
.cabecera {
  background-color: $cabecera-fondo;
  width: $cabecera-anchura/2;
  margin: 10px auto;
}
```



En relación con los valores que podemos tener podemos distinguir entre los siguiente tipos de datos:

- Números
- Cadenas
- Colores
- Booleanos
- Null
- Mapas y listas

Estos tipos de datos, salvo los últimos que explicaremos con más detenimiento, son comunes a muchos lenguajes de programación y son conceptos que se deberían manejar para poder realizar el curso.

No obstante podemos encontrar información adicional en el siguiente enlace de la documentación:

[Tipos de valores en Sass](https://sass-lang.com/documentation/values)

#### Valores por defecto y ámbito de las variables

Cuando estoy creando mi propia librería Sass y quiero permitir que los usuarios puedan personalizar las variables de la librería antes de generar el código CSS puedo dar a las variables valores por defecto.

Con esto consigo que al dar valor a una variable no se “machaque” el valor anterior. Se le asigne un valor sólo si no está definida y no es nula.

Se consigue de la siguiente manera:

```scss
// Código del usuario, previo al uso (o importación)
// de mi librería.
$gris: #ddd;
$color-letra-pie: $gris;

// Código de mi librería, respeto el anterior ya
// que está previamente definido.
$gris: #eee !default;
```



Las variables en Sass tienen ámbito, que no es más que el *“espacio”* en el cual su valoe este disponible. Así, en Sass podemos distinguir entre:

- ***Variables globales:\*** que estarán disponibles a lo largo de todo el código Sass y que se definen fuera de todo bloque, normalmente al principio de los ficheros Sass.
- ***Variables locales:\*** declaradas dentro de bloques (**{}**) y que solo están disponibles y tienen valor dentro de ese bloque.

Un ejemplo de estos dos tipos:

```scss
//Variable global fuera de todo bloque
$logo-width: 50%;

.header {
  //Variable local
  $header-width: 50%;
}
```



***NOTA:*** En caso de coincidir en nombre una variable local y una variable global prevalece en valor de la local.

### Comentarios

Los comentarios son un elemento fundamental en cualquier lenguaje de programación. Sass nos permite utilizarlos pudiendo distinguir entre:

1. Comentarios de una línea.
2. Comentarios multilínea.

Un ejemplo de cada uno de estos tipos lo podemos ver en el siguiente ejemplo:

```scss
//Este es un comentario de una línea

/*
  ...
  Este es un comentario multilínea
  ...
*/
```



Comentar bien es algo fundamental y es algo que se va aprendiendo con los años. No obstante os dejo aquí algunas recomendaciones básicas al respecto.

- Comenta todo lo que no sea evidente.
- Es casi imposible *comentar demasiado* , no te preocupes y explica lo que hagas.
- Describe las funciones que crees y utilices
- Explica las agrupaciones de las reglas y sus objetivos
- Describir los números mágicos.

### Listas y Mapas

Como hemos dicho anteriormente Sass nos proporciona la posibilidad de definir valores complejos como Listas y Mapas.

#### Listas

Las listas son colecciones de valores de datos y podemos declarar listas de la siguiente manera:

```scss
//Forma general de la definición
$variable_lista: (v1, v2, v3);

//Por ejemplo
$sizes: (40px, 80px, 160px);
//Otra posibilidad (conforme a las recomendaciones)
$sizes: (
  //
    40px,
  //
    80px,
  //
    160px
);
```



Asociadas a las listas existen multitud de funciones asociadas para acceder a los distintos elementos, añadir elementos, buscar un elemento etc...

[Funciones relacionadas con Listas](https://sass-lang.com/documentation/functions/list)

#### Mapas

Los mapas son colecciones de valores de datos a los que accedemos por clave. Podemos declarar listas de la siguiente manera:

```scss
//Forma general de la definición
$nombre_mapa: (
  //
    "clave1": valor1,
  //...
    "claveN": valorn
);

//Por ejemplo
$breakpoint: (
  "pequeño": 576px,
  "medio": 768px,
  "grande": 992px
);
```



Asociados a los mapas existen multitud de funciones asociadas para acceder a los distintos elementos, añadir elementos, buscar elementos etc…

[Funciones relacionadas con Mapas](https://sass-lang.com/documentation/functions/map)

### Interpolación

Una de las características más útiles y más usadas de Sass es la **Interpolación**.

La **Interpolación** nos permite, casi en cualquier sitio de un documento Sass, incrustar una expresión, cuyo resultado al ser evaluada formará un trozo de código CSS.

Para que esto ocurra debemos incluir la expresión de la siguiente manera:

```scss
  #{expresión_a_evaluar}
```



Algunos de los lugares donde podemos usar la interpolación son los siguientes:

- Selectores
- Nombres de propiedades
- Comentarios
- Reglas de Sass como @import, @extend y @mixins
- Cadenas con o sin comillas
- Funciones
- Etc…

A continuación vamos a ver algún ejemplo y cómo quedaría todo al generar el CSS:

```scss
  // Interpolación en selectores
  $buttton-type: "error";
  $btn-color : #f00;

  .btn-#{$name} {
      background-color:  $btn-color;
  }

  //Interpolación en el uso de funciones
  $fondo :  "images/fondos/default.png";

  .container {
    ...
    background-image: url("#{fondo}");
    ...
  }

  //Interpolación en comentarios
  $autor: "OpenWebinars";

  /* 
      Web desarrollada por #{$autor}
  */
```



### Anidamiento

Si estamos habituados a trabajar con CSS nos habremos dado cuenta de que conforme nuestras hojas de estilos se van haciendo más complejas nuestros selectores y sus niveles de anidamiento se van alargando.

La escritura de estos selectores es repetitiva, redundante y además no existe en nuestro código CSS ninguna relación de estos selectores, cosa que, en cuanto a significado si existe.

Sass nos permite anidar selectores para que, además de escribir menos, agrupemos selectores relacionados dentro de una misma organización sintáctica.

Lo podemos ver claramente en el siguiente ejemplo:

```scss
    //Estructura CSS para un menú simple
    nav {...}
    nav li {....}
    nav li a {...}

    //Alternativa usando las posiblidad es de anidamiento (Nesting) de Sass
    nav {
      ...
      li {
        ...
        a{...}
      }
    }
```



Podemos apreciar que este tipo de organización (nesting) es más concisa y agrupa lógicamente los selectores.

Además, podemos hacer referencia al selector padre usando el carácter ***&\***. Un ejemplo típico sería:

```scss
    //Estructura en CSS
    a {...}
    a:hover {...}

    //Alternativa en Sass usando & para referirse al selector padre
    a {
      ....
      &:hover {...}
    }
```



Podemos incluso usar ese carácter ***&\*** para crear nuevos selectores extendiendo el nombre del selector (ideal para metodologías de nombrado como BEM). Un ejemplo:

```scss
    //Selectores CSS
    .btn {...}
    .btn__warning {...}

    //Origen de selectores en SASS
    .btn {
      ...
      &__warning {...}
    }
```



### Mi primera compilación

Una vez hemos presentado los elementos más básicos que puede contener un fichero Sass vamos a ver cómo procedemos a generar el fichero CSS a partir de dicho fichero.

Recordad, tal y cómo hemos explicado anteriormente, cuál es el proceso.

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/proceso.png)

Y para compilarlo, desde nuestra terminal:

```shell
  > sass fichero_sass.scss fichera_salida.css
```

## Compilando archivos Sass


En el apartado anterior vimos que compilar archivos Sass consiste en la generación de manera automática del CSS correspondiente.

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/proceso.png)

En este apartado vamos a profundizar más en este proceso y vamos a ver que podemos realizar distintos tipos de compilación según nuestras necesidad.

### Tipos de compilación

A la hora de compilar tenemos muchas opciones pero para simplificar vamos a distinguir únicamente las siguientes:

- **Compilación simple:** donde a partir de un único fichero Sass generaremos un fichero CSS
- **Compilación múltiple:** donde partiendo de varios ficheros .scss podremos generar varios ficheros CSS
- **Compilación extendida:** es la opción por defecto, se genera un regla CSS por cada línea. Es la opción que se suele usar mientras estamos en fase de desarrollo.
- **Compilación comprimida:** generando una versión minimizada que elimina cualquier carácter que no sea imprescindible. Es la opción preferida una vez ya hemos pasado la fase de desarrollo y queremos subir nuestro CSS generado a producción.
- **Compilación que vigila los cambios :** Es una opción que le pasamos al compilador Sass para que al producirse los cambios en los archivos Scss se vayan regenerando automáticamente los ficheros CSS resultantes.

Un ejemplo de cada una de estas opciones:

```scss
//SIMPLE
sass file.scss output_file.scss

//MÚLTIPLE
sass file1.scss:output1.css ... fileN.scss:outputN.css

//EXPANDIDA (1 SELECTOR - 1 LÍNEA EN SALIDA - POR DEFECTO)
sass --style = expanded file.scss output_file.scss

//COMPRIMIDA (QUITA LA MAYOR CANTIDAD DE CARACTERES POSIBLES)
sass --style = compressed file.scss output_file.scss

//VIGILANDO LOS CAMBIOS Y ACTUALIZANDO FICHEROS
sass --watch file.scss output_file.scss
```



### Los ficheros .Map

Hasta ahora no habíamos hablado de ellos, pero si nos hemos fijado, al compilar nuestros ficheros Sass no se generan únicamente los ficheros CSS.

Al mismo tiempo que se generan esos ficheros CSS podemos ver que se crean unos archivvos intermedios con la extensión **.map**..

Esos ficheros contienen un ***mapeado*** de las reglas Sass a las reglas CSS.

De hecho, en los archivos CSS generados, y si usamos las opciones por defecto, podemos ver al final el archivo .map que contiene el mapeado.

### Otros *Flags* de compilación

Además de los flags que hemos visto en los ejemplos anteriores a la hora de compilar nuestros ficheros Sass podemos añadir otros *flags* que modificarán el comportamiento de la instalación:

- ***—no-source-map*** si no queremos que se generan los archivos .map al compilar.
- ***—stop-on-error*** si queremos parar el proceso de compilación cuando se produzca un error.
- ***—embed-source-map*** si queremos incluir el archivo .map directamente en el CSS generado y no en un fichero aparte.
- ***—help*** si queremos solicitar información adicional de cómo usar el compilador.
- ***—quiet*** si no queremos que el compilador muestre mensajes de advertencia (*warning*)
- ***—color/—no-color*** si queremos o no mostrar colores en los mensajes generados por el proceso de compilación.
- ***—trace*** si queremos mostrar toda la pila de llamadas del proceso de compilación cuando ha habido un error.

### Los comentarios al compilar

Un aspecto importante a tener en cuenta a la hora de compilar es si queremos incluir los comentarios en el archivo generado o no.

Y esto va a depender del tipo de compilación y del tipo de comentarios que utilizemos.

De manera general podemos describir esto con los siguiente ejemplos:

```scss
    //Este comentario no se incluye al general el archivo CSS

    /* Este comentario de varias líneas
       se incluye al genera el CSS salvo que la compilación la hagamos en modo 
       compressed (comprimido) */

       /*!
            Este comentario se incluye tabién en modo Compressed (comprimido)
       */
```

## Ejercicio práctico I

En este apartado vamos a realizar un pequeño ejercicio para poner en práctica lo aprendido hasta ahora. Tendremos que realizar lo siguiente:

- Usar los comentarios para describir nuestras reglas CSS.
- Una paleta de colores para distintos elementos de nuestra web.
- Estilos generales para el contenedor principal y los bordes de mis elementos.
- Incluir estructuras de anidamiento para crear estilos para un menú y una figura.

Aunque hemos explicado de manera previa los conceptos de listas, mapas e interpolaciones no los vamos a usar en este ejemplo si no que veremos cómo se usan de manera real cuando hablemos de estructuras de control. Tienen más sentido su uso junto a ese tipo de estructuras.

El objetivo es conseguir el siguiente resultado:

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/resultado.png)

### Paleta de colores

```scss
// Paletas de colores básica

//Colores generales
$color-background: #FFFFFF;
$color-base: #333333;


//Colores barra de menú
$navbar-background-color: rgb(155, 111, 145);
$navbar-color-hover: $color-background;

//Colores del borde 
$border-color: $color-base;
```



### Estilos generales para ciertos elementos

```scss
/*Estilos para el contenedor general de mi página*/
$main-container-width: 80%;

.container {
    width: $main-container-width;
    margin: 0 auto;
}


//Estilos para los bordes
$border-width: 0.1rem;
$border-style: "solid";

$border-radius-small: 0.2rem;
$border-radius: 1rem;
$boder-radius-big: 2rem;
```



### Estructuras de anidamiento

```scss
//Usando anidamiento crear la hoja de estilos para un menú 
nav {
    background-color: $color-navbar-background;
    padding: 0.2rem;

    ul {
        li {
            display: inline-block;
            padding-left: 1rem;
            padding-right: 1rem;

            &.active {
                color: $color-navbar-color-hover;
                font-weight: bolder;
            }

            &:hover {
                color: $color-navbar-color-hover;
            }


            a {
                text-decoration: none;
            }

        }
    }
}
```

 

```scss
/*Estructura de anidamiento para la figura (cards)*/
$figure-width: 25%;

figure.card {
    border: $border-width $border-style $border-color;
    border-radius: $border-radius;
    display: inline-block;
    padding: 1rem;

    width: $figure-width;
     img {
        border-radius: $boder-radius-big; 
        width: 100%;
     }

     figcaption {
         font-size: 0.8rem;
         margin: 0.5rem;
         text-align: center;
     }

}
```

## Estructuras de control y funciones

Además de los elementos básicos que vimos en el apartado 2.3 y, tal como ya dijimos anteriormente, **Sass** nos proporciona una serie de estructuras que, siendo típicas de los lenguajes de programación, nos van a ayudar mucho a la hora de desarrollar nuestras hojas de estilos en proyectos grandes.

En este apartado vamos a tratar dos:

- Estructuras de Control.
- Funciones.

### Estructuras de Control

Las estructuras de control son aquellas que nos van a permitir definir un flujo de ejecución a la hora de generar nuestras hojas de estilos. **Sass** nos proporciona las siguientes:

- @if / @else if / @else
- @while
- @for
- @each

#### @if / @else if / @else

Esta estructura nos permite controlar si un bloque **Sass** será evaluado o no atendiendo a una expresión o condición. De manera general tienen la siguiente estructura:

```scss
    @if expresion2 {
        //Se evalua si se cumple la expresión
    } @else if expresion2 {
        //Esta parte es opcional y se evalua si se cumple la expresión2    
    } @else {
        //Esta parte también es opcional. Se evalua cuando no se cumple ninguna de la expresiones de las pares anteriores.
    }
```



Un ejemplo sería:

```scss
// @if / @else if / @else example
$light-theme: true;
$dark-theme: false;

header {
  @if $light-theme == true {
    background-color: #fff;
    color: #000;
  } @else if $dark-theme {
    background-color: #000;
    color: #fff;
  } @else {
    //Default theme
    background-color: #aaa;
    color: #444;
  }
}
```



Dependiendo de los valores de las variables *$light-theme* y *$dark-theme* tendremos una u otra combinación de colores y, en caso de que ambas sean falsas, un combinación de colores por defecto.

#### @while

Esta estructura de control nos sirve para evaluar de manera repetitiva ciertas órdenes ***Sass\***. De manera general tiene la siguiente estructura:

```scss
  @while expresión {
      //Esto se evaluará mientras se cumpla la expresión
}
```



Un posible ejemplo sería:

```scss
$num: 1;
$color-list: #0f0, #00f, orange, #ccc;
@while $num < 5 {
  td:nth-child(#{$num}) {
    color: #f00;
    background-color: nth($color-list, $num);
  }
  $num: $num + 1;
}
```



Utilizamos el bucle para recorrer una lista de colores y hacer que las celdas de una tabla tengan diferentes colores dependiendo de la posición en la que se encuentran.

El Css que se genera sería el siguiente:

```scss
td:nth-child(1) {
  color: #f00;
  background-color: #0f0;
}

td:nth-child(2) {
  color: #f00;
  background-color: #00f;
}

td:nth-child(3) {
  color: #f00;
  background-color: orange;
}

td:nth-child(4) {
  color: #f00;
  background-color: #ccc;
}
```



#### @for

Esta estructura de control es muy similar a la estructura @while. Las diferencias son que nosotros establecemos unos valores inicial y final para controlar el número de veces que se repite la estructura y que no tenemos que preocuparnos para la actualización de las variables de control. De manera general tiene la siguiente estructura:

```scss
    @for $var from $start to $end {

    }
```



Un ejemplo sería:

```scss
num: 1;
$color-list: #0f0, #00f, orange, #ccc;

@for $i from 1 to 5 {
  p:nth-of-type(#{$i}) {
    color: #f00;
    background-color: nth($color-list, $i);
  }
}
```



Aquí estamos dando unos colores a los 4 primeros párrafos de una página web y escogiendo esos colores de una lista de colores. El Css que se genera es el siguiente:

```scss
p:nth-of-type(1) {
  color: #f00;
  background-color: #0f0;
}

p:nth-of-type(2) {
  color: #f00;
  background-color: #00f;
}

p:nth-of-type(3) {
  color: #f00;
  background-color: orange;
}

p:nth-of-type(4) {
  color: #f00;
  background-color: #ccc;
}
```



#### @each

Esta estructura es una estructura iterativa que utilizaremos para recorrer tanto listas como mapas.

#### @each para recorrer una lista

La estructura general sería:

```scss
$list: ...;

@each $e in $list {
    //Trozo de código que se evalua tantas veces como elementos tenga la lista. En cada iteración en $e tendremos el elemento de la lista que tratamos cada vez.
}
```



Un ejemplo:

```scss
$usuarios: pepe, lola,manuel;

@each $u in $usuarios {
   .profile-#{$e} {
    background: 
  image-url("img/#{$e}.png") no repeat;
}
```



Esto genera el siguiente Css:

```scss
.profile-pepe {
  background: image-url("img/pepe.png") no repeat;
}

.profile-lola {
  background: image-url("img/lola.png") no repeat;
}

.profile-manuel {
  background: image-url("img/manuel.png") no repeat;
}
```



***Nota:\*** Debemos tener cuidado con la utilización (o no), de las comillas en las listas.

#### @each para recorrer mapas.

En estos casos la estructura general sería:

```scss
$map: ...;

@each $k,$v in $map {
    //Trozo de código que se evalua tantas veces como elementos tenga el mapa. En cada iteración en $k tendremos la clave y en $v el valor del elemento del mapa
}
```



Un ejemplo sería:

```scss
$mapa : pepe : pepe.png, lola: lola.png ,manuel: manuel.png;

@each $u,$v in $mapa {
   .perfil-#{$u} {
    background: image-url("img/#{$v}") no repeat;
}
```



El Css resultante sería:

```scss
.perfil-pepe {
  background: image-url("img/pepe.png") no repeat;
}

.perfil-lola {
  background: image-url("img/lola.png") no repeat;
}

.perfil-manuel {
  background: image-url("img/manuel.png") no repeat;
}
```



### Funciones

Al igual que en un lenguaje de programación en **Sass** podemos definir funciones en las que por un lado pondremos, normalmente un trozo de código que vayamos a utilizar frecuentemente y , por otro lado, nos deben devolver un valor.

En Sass tendremos:

- Funciones definidas por nosotros mismos.
- Funciones nativas de Sass

#### Funciones definidas por el usuario

De manera general tienen la siguiente estructura:

```scss
//Función sin parámetros
@function name() {
    //Cuerpo de la función
    //que devolverá un valor
    @return ...;
}

//Función con parámetros (uno o varios)
@function name(p1,...,pn) {
    //Cuerpo de la función
    //que devolveŕa un valor
    @return ...;
}
```



Un ejemplo sería:

```scss
@function anchura-col($col,$total) {
  @return percentage($col/$total);
}

//Llamadas a la función
.sidebar {
    ...
    width: anchura-col(2,10);
    ...
}

.main {
    ...
    width: anchura-col(5,10);
    ...
}
```



El CSS generado:

```scss
.sidebar {
    ...
    width: 20%;
    ...
}

.main {
    ...
    width: 50%;
    ...
}
```



#### Funciones nativas

En relación a las funciones nativas decir que Sass nos proporciona un montón de utilidades en forma de función. En especial funciones:

- Numéricas
- Cadenas (Strings)
- Colores
- Listas
- Mapas
- Selectores
- Introspection

Podemos encontrar más información en la [documentación de Sass](https://sass-lang.com/documentation/functions).

## Ejercicio práctico II

Para seguir practicando e ir conociendo toda la potencia de Sass vamos a desarrollar en este ejemplo práctico lo siguiente.

- Un sistema para decoración de tablas con colores para las distintas columnas ya sean pares o impares (hasta 10 columnas) usando **@for** ,**@if** y **@function**.
- Un sistema de botones para tener botones de error / warning / accepted / normal usando **mapas** y **@each**.
- Un sistema de maquetación de hasta 8 columnas usando **@for** y **@function**.

### Decoración de tablas

```scss
//Estilos genreales para la tabla
table {
    border-collapse: collapse;
}

th,td {
    border: 1px solid black;
    padding: 1rem;   

}

//Colores para los columnas
$color-col-pares: #CCCCCC;
$color-col-impares: #888888;


//Función que me devuelve el color de fondo de las columnas dependiendo de si es para o impar
@function colum-color($col-number) {
    @if ($col-number%2 == 0) {
        @return $color-col-pares;
    } @else {
        @return $color-col-impares;
    }
}

//Establezco el número máximo de columnas
$inicio : 1;
$fin: 10;

tbody  tr td:hover {
    background-color: orange;
}

@for $num from $inicio to $fin {
    tbody tr  td:nth-child(#{$num}) {        
        background-color: colum-color($num);             
    }
}
```



### Sistema de botones

```scss
//Colores de base
$color-error: #FF0000;
$color-warning: rgb(239, 241, 120);
$color-accepted: rgb(55, 138, 0);
$color-normal: rgb(0, 110, 255);
$color-shadow: #888;

//Valor del radio para redondear los botones
$border-radius: 0.4rem;

//Mapa del que cogeremos los nombres para interpolarlos y los valores de los colores
$btn-colors: ("error" : $color-error,
    "warning" : $color-warning,
    "accepted" : $color-accepted,
    "normal" : $color-normal);

//Elemento botón general
.btn {
    display: inline-block;
    text-align: center;
    text-decoration: none;
    border-radius: $border-radius;
    padding: 0.5rem;
    //Estilo para el efecto de cuando pase el ratón por encima
    &:hover {
        box-shadow: $border-radius/2 $border-radius/2 $color-shadow;
    }
    //Estilo al hacer click en el boton
    &:active {
        background-color: orange;
        color: white;
    }
}

//Clase adicional que le da color
@each $k,
$v in $btn-colors {

    .btn-#{$k} {
        background-color: $v;
    }
}
```



### Sistema de maquetación de 8 columnas

```scss
//Sistema de maquetación

//Padre de cada fila
.row {
  display: flex;
  flex-direction: row;
}

//Les doy un tamaño y un borde para distinguirlos
//Sólo válida para el ejemplo
.row > * {
  border: 1px solid black;
  height: 150px;
}

//Número de elementos máximos que voy a tener
$num_elementos: 8;

//Función que devuelve la anchura correspondiente al elemento
@function anchura_col($i) {
  @return (100 / $num_elementos)*$i ;
}


//Bucle para generar las clases
@for $i from 1 through $num_elementos {
.row > .col-#{$i} {
    padding: 1rem;
    width: #{anchura_col($i)}+ "%";
  }
}
```

## Reglas y directivas

Ya hemos visto algunas directivas *@nombre_directiva* de Sass cuando hemos hablado de estructuras de control y de funciones pero, además, **Sass** nos proporciona algunas más que nos van a permitir

Entre ellas:

- @import
- @extend
- @error / @warn / @debug
- @at-root
- @media / @support / @keyframe

### @import

Desde mi punto de vista es una de las directivas más importantes y nos va a permitir construir una hoja de estilos combinando distintos archivos en tiempo de compilación, ya sean CSS o SCSS y que llamaremos ***partials\***.

De esta manera podré evitar distintas peticiones HTTP para cada una de las hojas de estilos.

Al realizar esta combinación desde esa hoja de estilos generada, que es una combinación de todos los partials, podremos acceder a las variables, a las funciones y a todas las partes de todos y cada uno de los archivos que compongan el archivo resultante.

De manera general usaremos la directiva de la siguiente manera:

```scss
@import "file1";
//...
@import "file2";
//@import "file1","file2"; sería los mismo

//Los ficheros (partials) tendrán que tener los siguientes nombres
//_file1.scss y _file2.scss
```

Un ejemplo de esto podría ser:

```scss
@import "scss/colors.scss";
@import "scss/layout.scss";
```

### @extend

Mediante el uso de la directiva @extend podremos reutilizar estilos (fragmentos de código) de tal manera que podré construir estilos de unos componentes usando como base los estilos de otros.

La sintaxis para usar esta directiva es la siguiente:

```scss
.some_selector  {
    //Estilos para este selector
}

.another_selector {
    @extend .some_selector;
    //Estilos propios de .another_selector
}
```

En este caso ***.another_selector\*** tendrá todos los estilos propios además de los estilos de ***.some_selector\***.

Un ejemplo típico del uso de esta directiva es la construcción de distintos tipos de botones que tiene la misma forma pero distinto color de fondo:

```scss
.btn {
    border-radius: 2px;
    color: #FFF;
    padding: 5px 0;
    margin: 2px auto;
    text-align: left;
    width: 150px;
}

.btn-error {
    @extend .btn;  
    background-color: #F00;  
}

.btn-ok {
    @extend .btn;
    background-color: #0F0;
}
```

Puede ser que queramos que uno de los selectores usados no salga en el CSS resultante pero que sí sea usado como base usando la directiva @extend. Para ello tendremos que utilizar un ***placeholder selector\*** que funciona igual pero tenemos que añadir el carácter ***%\*** delante del nombre del selector. Un ejemplo:

```scss
%btn {

}

.btn-error {
    @extend %btn;  
    background-color: #F00;  
}

.btn-ok {
    @extend %btn;
    background-color: #0F0;
}
```



Esta directiva, para usos avanzados tiene algunas peculiaridades que deben ser consultadas en la documentación oficial, pero de manera general podemos hacer algunos apuntes:

- La directiva @ se procesan después del resto (incluidos los selectores referentes a padres o parentescos).
- Copia el estilo en la regla actual.
- Los estilos añadidos tienen preferencia, igual que en HTML+CSS puro.
- Está a limitado a selectores simples (dart-version)
- Usado dentro de una directiva @media solo podremos hacer referencia a selectores dentro de esa directiva.
- Similar a @mixin (comparativa en el capítulo posterior).

### @error / @warning / @debug

Las directivas ***@error\***, ***@warn\*** y ***@debug** son directivas útiles cuando estamos depurando nuestro SCSS. Nos permiten mostrar mensajes y valores de variables, funciones etc..durante el proceso de generación del CSS.

Tienen una sintaxis muy sencilla:

```scss
@error <expression>; //PARA LA COMPILACIÓN

@warn <expression>;

@debug <expression>;
```



Un posible ejemplo sería:

```scss
$test: false;

body {
  @if $test {
    @error "Mensaje de error";
    @debug "Test tiene el siguiente valor: #{$test}"; //nuca sale
  }
  else {
    @warn "Mensaje de warning";
    @debug "Test tiene el siguiente valor: #{$test}"; //Sale cuando test es false
  }
}
```



### @at-root

La directiva ***@at-root\*** permite que selectores dentro de reglas con anidamiento sean *movidos* a la raíz del documento. Su uso no es algo común y se puede llegar a usar con anidamientos avanzados que usan funciones de selección y selectores referentes a elementos padre. Tiene sentido si seguimos metodologías de nombrado como [BEM](http://getbem.com/).

Tiene una sintaxis muy sencilla:

```scss
@at-root selector {
    //Estilos para ese elemento
}
```



Y una aplicación podría ser la siguiente:

```scss
.item {
    color: black;    
    @at-root #{&}is-active {
      color: blue;
    }
}
```



Que genera el siguiente CSS:

```scss
.item {
  color: black;
}
.item.is-active {
  color: blue;
}
```



Vemos con el selector sale a la raíz del documento y genera un selector que une los dos, que es el usado para aquellos elementos que tienen esas dos clases a la vez.

### @media / @support / @keyframes

Sass soporta las directivas @media, @support y @keyframes que son directivas propias de CSS.

## Mixins

Los mixins son un de las directivas más importantes que nos proporciona ***Sass\***.

¿Por qué es tan importante?. Pues porque nos permite definir estilos que luego voy a poder **reutilizar** a lo largo de la creación de mi hoja de estilos.

### Definición y uso de los mixins, la directiva @include

La definición de un mixin y su posterior uso es algo muy sencillo:

```scss
//Definición del mixin
@mixin nombre_del_mixin {
  //Contenido (estilos / etiquetas con estilos etc..)
}

//Uso del mixin
@include nombre_del_mixin;
```



Un ejemplo básico sería:

```scss
@mixin centrado {
  margin: 0px auto;
}

header {
  @include centrado;

  background-color: black;
  color: white;
}
```



Generando el siguiente CSS:

```scss
.header {
  margin: 0px auto;
  background-color: black;
  color: white;
}
```



Puedo incluir selectores dentro del *mixin* de tal manera que se pueda usar desde fuera de cualquier otro selector.
Por ejemplo:

```scss
@mixin highlighted-link {
  a {
    background-color: yellow;
    font-style: italic;
    text-decoration: none;
  }
}
@include highlighted-link; //Todo el contenido del mixin de muestra en el css
```

Y una de las características más útiles de los *mixins* es que podemos anidarlos usando la directiva @include dentro de una otra definición de mixin que a su vez, podremos usar posteriormente en otro lugar de nuestras hojas de estilos. Un ejemplo de ese uso podría ser:

```scss
@mixin centrado_menu {
  @include centrado;

  background-color: #666;
  color: white;
  height: 3rem;
}

.main_menu {
  @include centrado_menu();
}
```



### Mixins con parámetros

La verdadera potencia del uso de los ***mixins\***, además de la reutilización de nuestros estilos, reside en la posibilidad que vamos a tener de parametrizarlos. Cada mixins podrá recibir una serie se parámetros, que serán obligatorios u opcionales. En caso de ser opcionales tendremos que asignarles en la definición un valor por defecto.

Lo vamos a ver de manera muy claro con un ejemplo:

```scss
//Mixin con todos los parámetros obligatorios
@mixin girar($grados) {
  -webkit-transfrom: rotate(#{gradosdeg}deg);
  -ms-transform: rotate(#{gradosdeg}deg);
  transform: rotate(#{gradosdeg}deg);
}

//Mixin con parámetros opcionales y valores por defecto
@mixin logo($image, $radio: 10px) {
  background-image: url("#{$image}");
  background-position: center;
  border-radius: $radio;
}

//Especificando un valor
.logo-cuadrado {
  @include logo("img/milogocuadrado.png", 0px);
}

//Usando el valor por defecto del parámetro opciones
.logo-redondeado {
  @include logo("img/milogoredondeado.png");
}
```



### Mixins Vs Extends

Si os habéis dado cuenta ,la filosofía de la directiva **@mixin** es **similar** a lo que se pretende conseguir con la directiva **@extend**. El objetivo de ambas es conseguir la **reutilización de código**. Sin embargo tienen algunas diferencias que debemos mencionar y que tenemos que tener en cuenta cuando estemos diseñando nuestras hojas de estilos usando CSS.

- @mixin nos va a permitir el uso de parámetros mientras que @extend no. Eso hace que el uso de @mixin nos de más flexibilidad.
- Con @extend podemos combinar selectores, con @mixin no.
- @extend *puede* considerarse más adecuado si lo único que queremos es replicar estilos.
- Si usamos @extend con clases y subclases (coge todos los estilos) podemos encontrarnos con que es difícil de prever el código que vamos a generar. Eso es bastante peligroso en sí mismo

### Usos comunes para los mixins

Hay muchas posibilidades para el uso de la directiva @mixin pero a continuación vamos a citar algunos de los más comunes:

- Media Queries.
- Prefijos relativos a los navegadores.
- Transformaciones CSS.
- Animaciones CSS.
- Colocación fija de elementos.
- Gradientes.

Por si es de vuestro interés existe una página que los recopila por temática.

[Ejemplos de Sass Mixins](https://openwebinars.net/academia/aprende/sass/7172/!https://github.com/7ninjas/scss-mixins)

## Documentando Sass con SassDoc

Como ya comenté al principio de este curso, uno de los objetivos principales del curso era crear un entorno de desarrollo CSS más profesional. Con esa idea, uno de los puntos que no podemos dejar atrás es el hecho de que debemos documentar los que vayamos haciendo. Documentar es tedioso y que cuesta mucho esfuerzo pero, por suerte, existen herramientas como **SassDoc**

**SassDoc** es un sistema de documentación que me va a permitir **generar** de manera **automática** documentación en formato HTML referente a los ficheros SCSS en los que estoy trabajando. Siempre, evidentemente, siguiendo ciertas reglas que comentaremos posteriormente.

Hay muchas razones por las que debemos usar este tipo de herramientas entre ellas:

- Son herramientas profesionales y nosotros queremos ser profesionales (recordad el nombre del curso).
- Es ideal para proyecto grandes y complejos en los que trabaja mucha gente y en los que la existencia de documentación es algo vital.
- La documentación, no hay que olvidarlo, es parte del desarrollo aunque normalmente no nos guste mucho a los desarrolladores.
- Me permite gestionar automáticamente las dependencias de los elementos.
- Puedo integrarlo con Gulp que es el gestor de tareas que veremos luego y que me va a permitir construir mi workflow de trabajo.
- Puedo darle distintos temas visuales (predefinidos o creados por el usuario).

### Requisitos

Para poder trabajar con SassDoc necesitamos el entorno NodeJs instalado. Es decir:

- **Nodejs**: Entorno de ejecución para JavaScript.
- **Npm**: Gestor de paquetes para Node.
- **Npx**: Para poder ejecutar paquetes Node.

Para poder instalar todo esto tenemos las instrucciones detalladas, para todos los sistemas operativos, en el siguiente enlace.

[Instalación de Nodejs y asociados](https://nodejs.org/es/download/).

Podremos comprobar que todo está bien instalado con los siguientes comandos (pongo las versiones actuales):

```bash
> node --version
v10.16.3
> npm --version
6.9.0
> npm --version
6.9.0
```



### Instalación de SassDoc

Una vez hemos instalado los requisitos necesarios la instalación de **SassDocs** es muy sencilla. Debemos hacer lo siguiente:

```bash
## Para instalarlo a nivel global debemos de tener los permisos necesarios
> npm install sassdoc -g

## Para comprobar que la instalación se ha llevado a cabo correctamente
> sassdoc --version
2.71
```



### Uso básico

Para generar la documentación automáticamente con **SassDoc** debo:

- En primer lugar añadiré **comentarios a mis ficheros Sass**. Los comentarios que queramos incluir en la documentación generada por SassDoc deberán comenzar con ***///\*** (tres barras invertidas).
- Estos comentarios se colocarán encima de los elementos que queramos documentar.
- Una vez hayamos añadido todos los comentarios necesarios tendremos que ejecutar el siguiente comando:

```shell
> sassdoc ruta_proyecto_sass [opciones]
```



Esto generará por defecto la carpeta *sassdoc* con toda la documentación generada automáticamente siendo el punto de entrada el fichero *index.html*.

Y, ¿qué debemos comentar?. Pues normalmente en la documentación deberemos incluir información relativa a variables, funciones y mixins.

Dentro de los comentarios de SassDoc podemos añadir anotaciones que nos van a permitir dotar de significado a ciertos elementos de la documentación, agrupar elementos o simplemente darles un formato diferente (entre muchas otras cosas). Estas anotaciones comienzan por el carácter ***@\*** y su lista completa es la siguiente.

| C1          | C2         | C3       |
| ----------- | ---------- | -------- |
| @access     | @ignore    | @return  |
| @alias      | @link      | @see     |
| @author     | @name      | @since   |
| @content    | @output    | @content |
| @deprecated | @parameter | @throw   |
| @example    | @property  | @todo    |
| @group      | @require   | @type    |

Cada una de ellas tienen sus propias características y uso y aunque en el ejemplo no usaré todas en el siguiente enlaces tenéis la documentación exhaustiva:

[Anotaciones SassDoc Referencia](http://sassdoc.com/annotations/).

## Ejercicio práctico III

Para finalizar el apartado de Sass vamos a practicar lo que hemos visto en los apartado anteriores. Para ello el objetivo es que realicemos las siguiente taras:

- Estructurar todo los archivos que hemos realizado en distintos ficheros temáticos y usar una hoja de estilos y una fuente externa usando la directiva @import.
- Mejorar el sistema de maquetación creando dos breakpoints y usando la directiva @media.
- Crear mixins para prefijar ciertas propiedades y para realizar transformaciones que probaremos en una galería de imágenes.
- Documentar todo usando SassDoc

# GULP

## Instalación de Gulp

Hemos acabado el capítulo anterior viendo cómo documentar mi proyecto Sass. Hemos visto que a la vez que desarrollamos el proyecto debemos documentarlo,por una serie de razones que ya hemos explicado.

Ahora, a mi entorno de trabajo voy a añadir un herramienta más, **Gulp**.Pero, ***¿qué es Gulp?\***.

**Gulp** es una herramienta que entra dentro de la categoría ***Build-Tool\*** o ***Task-Runner\***. Nos va a permitir generar nuestra página web mediante la especificación de una serie de tareas que definiremos construyendo, de esta manera, nuestro **Workflow** de trabajo.

### Requisitos

Los requisitos para poder instalar **Gulp** son los mismos que para instalar **SassDoc**. Es decir:

- **Nodejs**: Entorno de ejecución para JavaScript.
- **Npm**: Gestor de paquetes para Node.
- **Npx**: Para poder ejecutar paquetes Node.

En el capítulo anterior ya vimos cómo hacerlo y cuál era la paǵina con los instaladores y la documentación necesaria para ello.

### Instalación

Una vez cumplidos con los requisitos instalar Gulp es tan sencillo como instalar un paquete node. Es decir:

```bash
## Si tengo los permisos adecuados
> npm install gulp -g

## Si tenemos que pedir permisos de superusuarios (Linux)
> sudo npm install gulp -g
```



Para comprobar que lo hemos instalado:

```bash
> gulp --version
CLI version: 2.2.0
```



### Crear un proyecto Gulp

Una vez tenemos Gulp instalado debemos de proceder,si es que estamos creando el proyecto desde cero, a crear el proyecto Gulp. Para ello desde la carpeta en la que estemos debemos ejecutar la siguiente orden:

```bash
> npm init
```

Esta orden me solicitará preguntas para la creación del fichero package.json dentro de esa carpeta.

Una vez hemos hecho esto debemos instalar Gulp localmente y hacer que el uso de Gulp se incluya en las dependencias de mi proyecto, lo que será necesario si queremos distribuir el mismo. Para ello:

```bash
> npm install gulp --save-dev
```

### El fichero package.json

El fichero ***package.json\***, que se crea al hacer *npm init*, es el manifiesto de nuestro proyecto y contiene toda la información del mismo así como las dependencias que pueda tener.

En el apartado anterior lo hemos creado, rellenando la información y añadiendo como dependencia ***Gulp\***. Tras hacer todo esto el contenido del mismo debería ser algo similar a lo siguiente:

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/package_json.png)

## Gulpfile.js y tareas Gulp

En el apartado anterior dejamos preparado todo el entorno para poder trabajar con ***Gulp\***. Creamos el proyecto, instalamos la dependencia de **Gulp** para el mismo e investigamos en contenido del fichero package.json que se se había creado al hacer estas dos operaciones.

Ahora vamos a empezar a utilizar **Gulp**. Si recordamos la definición era un “Task Runner” o “Build Tool”, es decir que lo que vamos a hacer con Gulp es definir una serie de tareas que **Gulp** ejecutará automáticamente con el objetivo de construir el **Workflow** (flujo de trabajo) para mi proyecto Front-End.

### Mi primer gulpfile.js

Sin anestesia…

```js
require("gulp");

//TAREA
function holamundo(cb) {
  console.log("Hola Mundo");
  cb();
}

//TAREA
function adiosmundo(cb) {
  console.log("Adios Mundo");
  cb();
}

//HACEMOS PUBLICAS LAS TAREAS
exports.holamundo = holamundo;
exports.adiosmundo = adiosmundo;
//ESTABLEZCO TAREA POR DEFECTO
exports.default = holamundo;
```



En primer lugar destacar que las sintaxis de esta versión del Gulp (4.0.2) difiere bastante con respecto a versiones anteriores. Es algo a tener en cuenta si buscáis tutoriales para aprender por vuestra cuenta.

Si sabemos un poco de javascript todo será mucho más fácil de entender pero básicamente lo que hacemos en ese fichero lo podemos dividir en tres partes:

- Inclusión del módulo Gulp para hacerlo disponible.
- Declaración de las tareas que vamos a tener como si fueran funciones javascript.
- Exportamos las tareas que consideramos que deben ser públicas (ejecutables desde el exterior) y declaramos la tarea por defecto (default).

### Ejecutando las tareas del gulpfile.js

Una vez hemos creado ese fichero podremos ejecutar las tareas e interactuar con el mismo a través de la línea de comandos. Siguiente con el ejemplo anterior:

```bash
### Lista las tareas contenidos en el fichero gulpfile.js de ese directorio
> gulp --task

### Ejecuta la tarea por defecto (default)
> gulp

### Ejecuta las tareas especificando el nombre
> gulp holamundo
> gulp adiosmundo
```



## Ejecutar tareas en serie y en paralelo

Para flujos de trabajo complejos Gulp nos permite realizar tareas unas detrás de otra, estableciendo una secuencia e incluso, dadas las características de javascript, lanzar tareas en paralelo para que se vayan ejecutando a la vez.

Probablemente al comienzo nuestros flujos de trabajo no vayan a ser muy complejos y debemos de tener cuidado con las dependencias y posibles condiciones de carrera que puedan tener nuestras tareas en paralelo.

La sintaxis de Gulp para declarar ejecuciones de tareas en paralelo y en serie es la siguiente:

```js
//Asignación desestructurada
const { series, parallel } = require("gulp");

function t1(cb) {
  //Contenido de la tarea t1
  cb();
}

function t2(cb) {
  //Contenido de la tarea t2
  cb();
}

exports.paralelo = parallel(t1, t2);
exports.default = series(t1, t2);
```



Es importante destacar que puede tener más de dos en serie y más de dos en paralelo y que puedo anidar tareas en serie y en paralelo para flujos complejos. Para más información deberíamos leer la documentación oficial.

### Creando un Pipeline. src() y dest()

Con los métodos src() y dest() de Gulp podemos procesar ficheros y construir pipelines de tal manera que cogiendo unos ficheros de origen los procesaremos usando algún plugin y el resultado será procesado por otros plugins y así sucesivamente hasta colocar los ficheros resultantes en su destino.

Un ejemplo básico sería el siguiente:

```js
const { src, dest } = require("gulp");

exports.default = function() {
  //Se mueven los ficheros js dentro de la carpeta src a la carpeta output
  return src("css/*.js").pipe(dest("dist/"));
};
```



En el anterior ejemplo no hay procesamiento ninguno. Existe únicamente un movimiento de ficheros desde una localización a otra. Sin embargo, entre esos dos pasos podemos añadir el procesamiento de los ficheros de origen por tantos plugins como queramos. Muchos de los plugins que nos interesan los veremos en el capítulo posteriormente pero la estructura general será la siguiente:

```js
const { src, dest } = require('gulp');

exports.default = function() {
  //Se mueven los ficheros js dentro de la carpeta src a la carpeta output
  return src('css/*.js')
  .pipe(plugin1())
  ....
  ....
  .pipe(plugin())
  .pipe(dest('dist/'));
}
```



### Reaccionando a cambios (watch)

Si no queremos estar llamando a tareas Gulp cada vez que realizamos un cambio en los ficheros el API de Gulp nos proporciona el método **watch()** que ejecutará ciertas tareas de manera automática si ciertos ficheros cambian.

La sintaxis es sencilla y podemos verla directamente con un ejemplo:

```js
//Obtengo la referencia
const { watch } = require("gulp");

exports.default = function() {
  //Cada vez que cambia un archivo scss se vuelven a generar todos los estilos
  watch("scss/*.scss", compilar_scss);
};
```

## Plugins Gulp

**Gulp** nos proporciona una serie de plugins que nos van a permitir realizar tareas dentro de nuestro *workflow* o *pipeline* para llegar a construir e incluso desplegar nuestra página web.

Recordad, que de manera general nuestro *workflow* de trabajo va a ser algo similar a lo que podemos ver aquí abajo.

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/workflow.png)

Pero, ¿dónde podemos encontrar todos esos plugins que nos van a facilitar el desarrollo web?.

Tenemos la suerte de que en la misma página oficial de [Gulp](https://gulpjs.com/) tenemos un recopilatorio de más de 3700 plugins con todo tipo de funcionalidad. Dada esa variedad la misma página nos proporciona un buscador para hacer un poco más fácil la búsqueda. Podemos acceder a la lista completa y al buscador en el siguiente [enlace](https://gulpjs.com/plugins/).

### Instalación de los plugins

Una vez hemos localizado el plugin que nos interesa nos daremos cuenta de al hacer click sobre el nombre nos lleva a la página del [gestor de paquetes de Node (npm)](https://www.npmjs.com/). En dicha página se recoge la instalación y de cada uno de ellos aunque, para simplificar, podemos decir que la instalación de un plugin se realizará de la siguiente manera:

```shell
## Si queremos que la instalación sea global
> npm install -g nombre_del_modulo_o_plugin

## Si queremos que la dependencia se guarde en el package.json para la posterior distribución de mi proyecto
> npm install --save-dev nombre_del_modulo_o_plugin
```



### Algunos plugins o módulos recomendados

La lista es larga, evoluciona constantemente pero para las tareas relacionadas con los contenidos del curso podemos establecer una lista más pequeña:

- **gulp-concat:** Concatenación de ficheros.
- **gulp-if:** Ejecución condicional de tareas en Gulp.
- **gulp-rename:** Renombrado de ficheros.
- **gulp-dart-sass:** Compilación de Dart-Sass(gulp-sass para versiones anteriores de Sass).
- **gulp-processhtml:** Procesamiento y modificación de ficheros HTML.
- **gulp-plumber:** Gestión de los errores en la ejecución de las tareas de un pipeline o workflow.
- **del:** Borrado de ficheros.
- **path:** Trabajar con rutas a ficheros y/o directorios.
- **gulp-image-optimize:** Para optimizar el tamaño de los ficheros de image.
- **gulp-pleease:** Postproceso de ficheros CSS.
- **sassdocs:** Documentación de ficheros SCSS utilizando ficheros SassDoc.
- **gulp-ssh:** Para gestionar conexiones y transferencias sftp o ssh.

Existen, por supuesto, muchas más y sobre todo, son interesantes para el desarrollo Front-End aquellas relacionadas con JavaScript, pero el desarrollo JavaScript queda fuera del alcance de este curso.

### Ejemplos de uso

A continuación vamos a ver ejemplos para ficheros gulpfile.js que usan algunos de estos plugins para ejecutar tareas.

#### Ejemplo con *gulp-concat*

```js
//Defino una tarea que devuelve un stream que concatena todos los
// archivos de la carpeta css en un solo archivo final.css y lo deja en
// la misma carpeta
function concatenar() {
  return src("./css/*")
    .pipe(concat("final.css"))
    .pipe(dest("./css/"));
}

exports.contatenar = concatenar;
```



#### Ejemplo con *gulp-pleeease* y *gulp-rename*

```js
///Defino una tarea que devuelve un stream que minimiza el contenido del fichero final.css, le cambia el nombre poniendo el sufijo .min y lo deja en la misma carpeta
function min_and_rename() {
  return src("./css/final.css")
    .pipe(pleeease())
    .pipe(
      rename({
        suffix: ".min",
        extname: ".css"
      })
    )
    .pipe(dest("./css/"));
}
```



### Ejemplo con *del*

```js
//Borra el archivo final generado (callbacks). En este caso no hay stream
function borrado (cb) {
  del("./dist/css/final.css");
  cb();
}
```



#### Ejemplo con *gulp-dart-sass* y *sassdoc*

```js
//Tarea para generar el CSS a partir de los ficheros Sass
function generar() {
  return src("./scss/styles.scss")
  .pipe(sass())
  .pipe(dest('./dist/css/'));
}

//Opciones para el módulo sassdoc
var doc_options = {
  dest : 'docs' //Ruta de destino a la documentación
}

//Tarea para generar los documentos de los ficheros Scss
function generar_docs() {
  return src("./scss/styles.scss")
  .pipe(sassdoc(doc_options));
}
```



#### Ejemplo con *gulp-if*

```js
//Opciones que vamos a usar como condiciones del plugin gulpif
var options = {
  minimize : false,
  rename : true
}

function min_and_rename() {
  return src("./dist/css/final.css")
    // Si minimize es true se aplica la tarea
    .pipe(gulpif(options.minimize,pleeease()))
    // Si la opción de rename es true se aplica la tarea de renombrado
    .pipe(gulpif(options.rename,
      rename({
        suffix: ".min",
        extname: ".css"
      })
    ) )   
    .pipe(dest("./dist/css/"));
}
```

## Workflow con Gulp

Una vez nos hemos acercado, aunque sea muy por encima, a las posibilidades que nos presenta Gulp vamos a definir cuál será nuestro “pequeño” flujo de trabajo como desarrolladores Front-End.

Pero antes de proseguir vamos a recordar lo que era un ***workflow\*** o *flujo de trabajo*.

> Un ***workflow\*** es una serie de pasos que conforman nuestro proceso de trabajo. En este caso nuestro trabajo como desarrollador FrontEnd.

En este capítulo lo definiremos y posteriormente, en el capítulo 5, lo usaremos en el apartado para el desarrollo de nuestro propio FrameWork CSS.

Tenemos que tener en cuenta que en este workflow, una vez estemos en una empresa, habrá más tareas que quedan fuera del alcance de este curso. Tareas como testing, creación de contenedores (si es que nuestra empresa los uso) y despliegues más complejos que el que vamos a proponer que es un despliegue tradicional mediante SSH / SFTP.

Una vez dicho esto la lista de tareas que deberemos hacer con Gulp es la siguiente:

1. **Generar** las hojas de estilos a partir de los ficheros .scss.
2. **Generar** la documentación de nuestro framework a partir de los comentarios en los ficheros .scss.
3. **Mover** todos los ficheros, incluidos los ficheros .html a la carpeta destino.
4. **Borrar** los contenidos de las carpetas destino antes de la generación de las hojas de estilos y de los documentos y de mover los ficheros
5. **Subir** la carpeta destino al servidor ya que mediante sFTP o mediante SSH.

### Tareas en Serie / Tareas en Paralelo

Debemos recordar también que Gulp podemos definir flujos en serie y flujos en paralelo.

Mediante los flujos en serie expresamos un orden de ejecución de tal manera que cuando un tarea acaba la siguiente dentro del flujo empieza.

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/serie.png)

Si embargo en los flujos en paralelo, en un momento dado de nuestro flujo de trabajo dos tareas se empiezan a realizar a la vez.

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/paralelo.png)

### Una visión global

Una vez dicho esto, el flujo de trabajo que os propongo es este, aunque podéis adaptarlo según vuestras propias necesidades.

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/workflow.png)

- Empezaremos borrando los contenidos de la carpeta destino.
- Generaremos las hojas de estilos y la documentación en paralelo.
- Moveremos lo generado a la carpeta destino.
- Una vez finalizado todo esto lo subiremos a nuestro servidor.

## Ejercicio práctico

Usando Gulp vamos a desarrollar el flujo de trabajo propuesto en el capítulo anterior.

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/workflow.png)

# CREACIÓN DE MI PROPIO TEMA BOOTSTRAP USANDO SASS

## Descargando BootStrap

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/logo.png)

Una vez nos hemos introducido en capítulos anteriores en el mundo de **Sass** y **Gulp** vamos a ver ahora cómo usar estas tecnologías para modificar **BootStrap** a nuestro gusto.

Pero, **¿qué es BootStrap?**.

**BootStrap** es un framework CSS desarrollado por la empresa Twitter que nos proporciona componentes web y que nos permite desarrollar páginas web responsivas de una forma rápida y sencilla.

Y, **¿por qué hemos elegido BootStrap para este curso?**.

Existen más alternativas pero hay tres razones de mucho peso (al menos desde mi punto de vista) que justifican esta elección:

- Es **fácil** de usar.
- Está muy ***bien documentado** y tiene una comunidad muy activa que lo soporta.
- Y la razón de más peso en relación con este curso, **USA SASS** para generar y mantener la hoja de estilos final.

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/love_sass.png)

Pero no todo es maravilloso, para mí el **principal inconveniente** de usar **BootStrap** es que la página que voy a obtener es **IGUAL** de apariencia a otras muchas que usan esta librería.

Sin embargo, al estar hecha con Sass va a ser mucho más fácil modificar esta apariencia y adaptar BootStrap a mis propias necesidades y gustos.

### Descargando BootStrap

Para empezar a trabajar con **BootStrap** debemos clonar el repositorio que se encuentra en la siguiente [dirección](https://github.com/twbs/bootstrap).

Para clonarlo usaremos git:

```bash
> git clone https://github.com/twbs/bootstrap.git
```

## Comprendiendo la organización del proyecto

Una vez nos hemos descargado BootStrap y antes de empezar a modificarlo para crear nuestro propio tema debemos comprender varias cosas:

- La estructura de directorios del repositorio que nos acabamos de bajar.
- El contenido del fichero package.json.
- El contenido y estructura de la carpeta Scss.

### Estructura de directorios

A nivel general la estructura del repositorio se puede apreciar en la siguiente imagen:

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/estructura_general.png)

Destacan, para nuestros objetivos, las siguientes carpetas:

- La carpeta **SCSS** que contiene todos los ficheros *Sass* para construir las hojas de estilos asociados al Framework ***BootStrap\***.
- La carpeta **JS** que contiene todos los archivos javascript asociados al comportamiento de los componentes que nos proporciona BootStrap (alertas, cuadros de diálogo, menú, galería de imágenes etc…)
- La carpeta **DIST** que es la carpeta destino, la carpeta donde se colocan todas las hojas de estilos y los archivos javascript una vez se ha realizado el proceso de “build”, el proceso de construcción.

Menos importante para nosotros, al menos para este curso son:

- La carpeta **SITE** que contiene la página de BootStrap que está desarrollado con [Hugo](https://gohugo.io/) un framework para la generación de sitios webs estáticos.
- La carpeta **BUILD** que contiene los scripts del proceso de Build que de forma nativa se hace con Script npm, aunque **nosotros usaremos Gulp**
- La carpeta **NUGET** que contiene la información relativa a *BootStrap* para la herramienta de Microsoft para la gestión de paquetes .Net [Nuget](https://www.nuget.org/).

### El fichero package.json

Es, como ya hemos dicho en capítulos anteriores, el fichero que se crea cuando trabajo con paquetes Node y contiene todo lo relativo a:

- Las dependencias necesarias para construir tanto BootStrap como su documentación.
- Los scripts para todas las tareas de ese proceso.

### El directorio Scss

Para nosotros, en este curso, es el directorio más importante ya que es el que contiene toda la información para la construcción de las hojas de estilos asociadas a **BootStrap**, tanto para todos aquellos aspectos relativos a la maquetación como para aquellos aspectos relacionados con la apariencia de los elementos.

El punto de partido es el fichero **bootstrap.scss** que contiene todo lo necesario, aunque hay versiones más limitadas como **bootstrap-grid.scss**, **bootstrap-reboot.scss** y **bootstrap-utilities.scss**.

En este fichero se añaden todos los elementos mediante el uso de la directiva Sass ***@import\***.

## Creando mi tema (Parte I)

Una vez nos hemos descargado BootStrap vamos a comenzar a modificar el tema por defecto poco a poco.El objetivo es que nuestra página sea diferente a las demás páginas que usan BootStrap y con ese objetivo vamos a dividir los contenidos de este apartado en los siguientes puntos:

1. Proyecto de Base
2. Elementos a modificar.
3. Localización de los ficheros a modificar.
4. Objetivo final.

### Proyecto Base

Para poder comprobar que estamos modificando el tema **BootStrap** correctamente vamos a ver los efectos de las hojas de estilos generadas en una página de ejemplo que tendrá la siguiente apariencia inicial con el tema BootStrap por defecto.

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/proyecto_base.png)

### Elementos a modificar

BootStrap 4, que es la versión que vamos a usar, es un FrameWork Css muy completo que nos proporciona un montón de utilidades, componentes y que nos hace muy fácil realizar páginas web responsivas.

No pretendo modificar todos los aspectos del mismo y sobre todo no quiero tocar los archivos que me permiten gestionar las responsividad. Voy a limitarme a los componentes y aspectos que están en la web que es mi base.

Por lo tanto voy a modificar el tema por defecto en los siguientes aspectos:

1. **Body** (el color de fondo de la página).
2. El tipo de **letra**.
3. La apariencia del **menú** (componente navbar).
4. La apariencia de las **listas desplegables** (componente dropdown).
5. La apariencia de los **formularios**.
6. La apariencia del **carrusel de imágenes**.
7. La apariencia de las **tarjetas** (componente card).
8. La apariencia de los elementos en **pestañas** (componente tab).
9. La apariencia de los componentes **list-group**.
10. La apariencia de los **cuadros modales** (componente modal).
11. La apariencia de las **alertas** (componente alert).

Como es mucho trabajo en este apartado vamos a cubrir la modificación del tema por defecto para los 5 primeros y en el siguiente apartado veremos los demás.

### Localización de los ficheros a modificar

Los ficheros a modificar son los ***archivos .scss\*** que están en la carpeta ***scss\*** del repositorio **BootStrap** que me he descargado en el apartado anterior.

**BootStrap** está muy bien organizado y sólo fijándonos en el nombre podemos saber qué estilos contiene cada uno de los ficheros ***.scss**.

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/ficheros_sass.png)

### Objetivo Final

Mi objetivo final, una vez modificados todos los ficheros .scss necesarios es obtener la siguiente web:

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/proyecto_base.png)

## Creando mi tema (Parte II)

Seguimos modificando los estilos por defecto de BootStrap.Ahora vamos a cambiarlos para los siguientes elementos.

- La apariencia del carrusel de imágenes.
- La apariencia de las tarjetas (componente card).
- La apariencia de los componentes list-group.
- La apariencia de los cuadros modales.
- La apariencia de las alertas.

## Generando un nuevo tema con Sass/Gulp

En los anteriores dos apartados hemos ido desarrollando el tema y viendo sus resultados en directo. Esto está bien para aprender pero cuando tengamos ya todo el proceso definido lo que queremos en generar un resultado de forma automática y con todo lo necesario empaquetado.

Para conseguir esto usaremos las herramientas Gulp que es la herramienta con la que hemos trabajado en el tema anterior.

En este caso las tareas que quiero que haga Gulp son las siguientes:

- Generar mi nueva de estilos BootStrap usando Sass.
- Minimizar esa hoja de estilos.
- Mover esa hoja de estilos generada a la carpeta del proyecto con el que estamos trabajando.
- Mover también a esa carpeta del proyecto todos los ficheros js (javaScript) de BootStrap). En este caso no los estoy usando pero puede que en un futuro quiera usarlos para trabajar con los componentes de BootStrap.
- Procesar el Html para enlazar correctamente la hoja de estilos.

Algunas de estas tareas las podremos paralelizar. Eso lo veremos cuando estemos creando mi fichero gulpfile.js

**NOTA:** Recordar que para poder trabajar con Gulp tendré que crear el proyecto e instalar gulp y los plugins que vaya a usar.

# MI PROPIO FRAMEWORK

## Estructura de mi proyecto

Vamos a ponernos manos a la obra. Vamos a desarrollar un pequeño FrameWork CSS usando las herramientas que hemos visto a lo largo del curso.

Usaremos **Gulp** para definir mi proceso, del que ya hablaremos más detenidamente en el próximo apartado, usaremos **Sass** para generar y mantener de manera correcta las hojas de estilos y usaremos **SassDoc** para generar de manera automática los documentos asociados al framework.

De momento, y para empezar, en este apartado describiremos la estructura de directorios del proyecto y enumeraremos los elementos que lo van a conformar.

Tened en cuenta que es un FrameWork sencillo construido para uso docente. Un framework de verdad tendría muchos más elementos y más complejidad.

### Estructura de directorios

Tendremos una estructura de datos inicial con las siguiente carpetas y ficheros:

- **scss:** Carpeta que contendrá todos los ficheros Sass..
- **js:** Carpeta que contendrá los archivos tjavascript de mi proyecto. En nuestro caso la librería de iconos ***FontAwesome\***.
- **vendors:** Hojas de estilos realizadas por terceros. En nuestro caso las hojas de reseteo.
- **dist:** Carpeta en la que colocaremos el resultado de la compilación **Sass** y **SassDoc**.
- **img:** Carpeta que contendrá las imágenes del proyecto de prueba.
- El fichero *index.html* que esl fichero que usaremos para poner en funcionamiento nuestro framework.

A su vez, una vez hemos compilado todos los ficheros para la construcción del Framework dentro de la carpeta **dist** tendremos las siguientes subcarpetas y ficheros:

- **css:** Hojas de estilos generados por Sass.
- **docs:** Documentos generados por SassDocs.
- **js:** Ficheros javascript necesarios para el proyecto.
- **img:** Carpeta que contendrá las imágenes del proyecto una vez hayan sido optimizadas.
- El fichero index.html con las hojas de estilos y los ficheros javascript perfectamente enlazados.

### Elementos del Framework

Siguiendo con la idea de un FrameWork sencillo vamos a tener los siguientes elementos.

En la Parte I:

- Hojas de reseteo de estilos.
- FontAwesome.
- Estilos para elementos generales.

En la Parte II:

- Layout / Maquetación.
- Tablas.
- Listas.

En la Parte III:

- Imágenes.
- Formularios.
- Menú.

## Workflow de Sass/Gulp

Para desarrollar nuestro proyecto vamos a usar como base el mismo workflow que desarrollamos en el apartado 3.5 de este curso.

![img](https://dc722jrlp2zu8.cloudfront.net/media/uploads/2019/11/13/workflow.png)

Y dentro de ese workflow vamos a distinguir las siguientes tareas:

- Borrado de los directorios destino cada vez que comencemos el proceso.
- Generar y minimizar el CSS partiendo de nuestros ficheros Sass.
- Generarar la documentación usando SassDoc.
- Mover los ficheros generados a su sitio adecuado (ahora tendré más ficheros).
- Preprocesar el fichero html de ejemplo para que todo funcione correctamente.
- Subir mi página al servidor.

Para ellos vamos a usar la misma lista de plugins para gulp que usamos en ese apartado:

- **delete**
- **gulp-dart-scss**
- **gulp-pleeease**
- **sassdoc**
- **gulp-rename**
- **gulp-ssh**
- **gulp-processhtml**
- **gulp-autoprefixer**

## Creando mi framework (Parte I)



## Creando mi framework (Parte II)

## Creando mi framework (Parte III)



## Usando mi propio framework

