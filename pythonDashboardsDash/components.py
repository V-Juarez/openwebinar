from jupyter_dash import JupyterDash
import dash_core_components as dcc
import dash_html_components as html
import plotly.graph_objects as go

app5 = JupyterDash(name)
app5.layout = html.Div([
html.Div([
html.Div([
html.H3('Column 1'),
dcc.Graph(id='g1', figure={'data': [{'y': [1, 2, 3]}]})
], className="six columns"),

    html.Div([
        html.H3(&#39;Column 2&#39;),
        dcc.Graph(id=&#39;g2&#39;, figure={&#39;data&#39;: [{&#39;y&#39;: [1, 2, 3]}]})
    ], className=&quot;six columns&quot;),
], className=&quot;row&quot;)
])
app5.run_server(mode='external')