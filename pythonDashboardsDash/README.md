<h1> Creación de dashboards con Dash</h1>

<h3>Abraham Requena Mesa</h3>

<h1>Tabla de Contenido</h1>

- [1. Introducción](#1-introducción)
  - [Presentación](#presentación)
  - [¿Qué es Dash?](#qué-es-dash)
  - [Preparación del entorno](#preparación-del-entorno)
  - [Mi primera app con Dash](#mi-primera-app-con-dash)
  - [Contenido adicional](#contenido-adicional)
- [2. Componentes principales de Dash](#2-componentes-principales-de-dash)
  - [Conociendo los componentes de Dash](#conociendo-los-componentes-de-dash)
  - [Dropdown y Slider](#dropdown-y-slider)
  - [RangeSlider e Input](#rangeslider-e-input)
  - [TextArea, checkbox y radio item](#textarea-checkbox-y-radio-item)
  - [Buttom, dates y markdown](#buttom-dates-y-markdown)
  - [Upload, pestañas y gráficos](#upload-pestañas-y-gráficos)
  - [Ejercicio con componentes (Parte I)](#ejercicio-con-componentes-parte-i)
  - [Ejercicio con componentes (Parte II)](#ejercicio-con-componentes-parte-ii)
  - [Ejercicio con componentes (Parte III)](#ejercicio-con-componentes-parte-iii)
  - [Contenido adicional](#contenido-adicional-1)
  - [4. Dashtable](#4-dashtable)
  - [Creación de DataTable](#creación-de-datatable)
  - [Ajustando el tamaño](#ajustando-el-tamaño)
  - [Estilos](#estilos)
  - [Ordenación y filtrado](#ordenación-y-filtrado)
  - [Ejercicio con DataTable (Parte I)](#ejercicio-con-datatable-parte-i)
  - [Ejercicio con DataTable (Parte II)](#ejercicio-con-datatable-parte-ii)
  - [Contenido adicional](#contenido-adicional-2)
- [4. Callbacks y estados](#4-callbacks-y-estados)
  - [¿Qué son los Callbacks y los Estados?](#qué-son-los-callbacks-y-los-estados)
  - [Ejemplo práctico con Callbacks](#ejemplo-práctico-con-callbacks)
  - [Ejemplo práctico con Estados](#ejemplo-práctico-con-estados)
  - [Contenido adicional](#contenido-adicional-3)
- [5. BootStrap](#5-bootstrap)
  - [¿Qué es BootStrap y por qué usarlo?](#qué-es-bootstrap-y-por-qué-usarlo)
  - [Componentes más usados (Parte I)](#componentes-más-usados-parte-i)
  - [Componentes más usados (Parte II)](#componentes-más-usados-parte-ii)
  - [Contenido adicional](#contenido-adicional-4)
- [6.Proyecto](#6proyecto)
  - [Definiendo el dashboard](#definiendo-el-dashboard)
  - [Creación de la aplicación y de los ficheros](#creación-de-la-aplicación-y-de-los-ficheros)
  - [Añadir título y Dropdown](#añadir-título-y-dropdown)
  - [Creación de DataTable](#creación-de-datatable-1)
  - [Creación de gráficos](#creación-de-gráficos)
  - [Creación de mapas](#creación-de-mapas)
  - [Creación de RangeSlider e Input](#creación-de-rangeslider-e-input)

# 1. Introducción
## Presentación
## ¿Qué es Dash?
## Preparación del entorno
## Mi primera app con Dash

Para poder realizar el curso necesitamos:

- Python, usaremos [Anaconda](https://www.anaconda.com/distribution/#download-section)
- Un IDE (en mi caso usaré [Visual Studio Code](https://code.visualstudio.com/download))

Una vez instalado ambas herramientas, pasamos a configurar nuestro proyecto.

- Creamos una carpeta raíz de nuestro proyecto.
- Abrimos dicha carpeta como workspace en VSCode.
- Creamos en enviroment con conda y comenzamos a instalar los paquetes que vamos a necesitar durante el curso.
  - jupyter lab
  - pandas
  - numpy
  - dash==1.0.2
  - dash-daq==0.1.0
  - dash-bootstrap-components

Para crear el nuevo enviroment, lanzamos:

```sh
conda create --name dash_openwebinars_env python=3.7
conda activate dash_openwebinars_env
```

Podemos listar los paquetes instalados con:

```sh
conda list
```

Pasamos a instalar todos los paquetes con conda.

```sh
y
```

Una vez instalado todos los paquetes, vamos a crear la estructura de nuestro proyecto, que inicialmente, será muy básica.

- Una carpeta ‘src’
- Un archivo ‘app.py’

Seleccionaremos el enviroment con el que desarrollaremos nuestro proyecto en la esquina inferior izquierda de VSCode. Puede ser que necesitemos reiniciar nuestro IDE para que aparezca.

Por último, vamos a preparar nuestro IDE para poder debugear nuestro código. Para ello, solo tenemos que añadir una configuración en el menú superior izquierdo. Luego, añadimos nuestro breakpoint, y lanzamos nuestra app.

# 2. Componentes principales de Dash

## Conociendo los componentes de Dash

Checkbox

CheckItems

Button

Data

Upload



## Dropdown y Slider



## RangeSlider e Input
## TextArea, checkbox y radio item
## Buttom, dates y markdown
## Upload, pestañas y gráficos
## Ejercicio con componentes (Parte I)
## Ejercicio con componentes (Parte II)

## Ejercicio con componentes (Parte III)

## Contenido adicional

# 3. Dashtable

## Creación de DataTable

## Ajustando el tamaño
## Estilos
## Ordenación y filtrado
## Ejercicio con DataTable (Parte I)
## Ejercicio con DataTable (Parte II)
## Contenido adicional
# 4. Callbacks y estados

## ¿Qué son los Callbacks y los Estados?
## Ejemplo práctico con Callbacks
## Ejemplo práctico con Estados
## Contenido adicional
# 5. BootStrap

## ¿Qué es BootStrap y por qué usarlo?
## Componentes más usados (Parte I)
## Componentes más usados (Parte II)
## Contenido adicional
# 6.Proyecto

## Definiendo el dashboard
## Creación de la aplicación y de los ficheros
## Añadir título y Dropdown
## Creación de DataTable
## Creación de gráficos
## Creación de mapas
## Creación de RangeSlider e Input
