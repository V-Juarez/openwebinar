Para crear el nuevo enviroment, lanzamos:

```sh
conda create --name dash_openwebinars_env python=3.7
```

Activate

```sh
conda activate dash_openwebinars_env
```

Deactivate

```sh
conda deactivate
```

instalar `requeriments.txt`

```sh
conda install --file requirements.txt
```

instalar paquetes

```sh
conda install -c conda-forge {package_name}
```