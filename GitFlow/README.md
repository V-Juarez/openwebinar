<h1>TEMARIO</h1>

- [INTRODUCCIÓN AL CURSO](#introducción-al-curso)
  - [Presentación](#presentación)
  - [REPASO DE GIT](#repaso-de-git)
  - [Repaso de Git](#repaso-de-git-1)
  - [¿Cómo funciona Git?](#cómo-funciona-git)
  - [INTRODUCCIÓN AL GITFLOW](#introducción-al-gitflow)
  - [Introducción al Gitflow](#introducción-al-gitflow-1)
- [FLUJO DE TRABAJO CON GITFLOW](#flujo-de-trabajo-con-gitflow)
  - [Dos tipos de ramas](#dos-tipos-de-ramas)
  - [Rama Master](#rama-master)
  - [Rama Develop](#rama-develop)
  - [Rama Feature](#rama-feature)
  - [Rama Release](#rama-release)
  - [Ramas Hotfix](#ramas-hotfix)
  - [Uniéndolo todo](#uniéndolo-todo)
  - [Adaptándolo a nuestro entorno](#adaptándolo-a-nuestro-entorno)
- [COMANDOS DE GITFLOW](#comandos-de-gitflow)
  - [Introducción a los comandos](#introducción-a-los-comandos)
  - [Instalación del paquete](#instalación-del-paquete)
  - [Comandos de las ramas Master y Develop](#comandos-de-las-ramas-master-y-develop)
  - [Comandos de las ramas Feature](#comandos-de-las-ramas-feature)
  - [Comandos de las ramas Release](#comandos-de-las-ramas-release)
  - [Comandos de las ramas Hotfix](#comandos-de-las-ramas-hotfix)
  - [Merges](#merges)
- [REVISIONES CRUZADAS](#revisiones-cruzadas)
  - [¿Qué son las revisiones cruzadas?](#qué-son-las-revisiones-cruzadas)
  - [Proceso](#proceso)
  - [Establece tus reglas](#establece-tus-reglas)
  - [Herramientas](#herramientas)
# INTRODUCCIÓN AL CURSO

## Presentación

## REPASO DE GIT

## Repaso de Git

## ¿Cómo funciona Git?

## INTRODUCCIÓN AL GITFLOW

## Introducción al Gitflow

# FLUJO DE TRABAJO CON GITFLOW

## Dos tipos de ramas

## Rama Master

## Rama Develop

## Rama Feature

## Rama Release

## Ramas Hotfix

## Uniéndolo todo

## Adaptándolo a nuestro entorno

# COMANDOS DE GITFLOW

## Introducción a los comandos

## Instalación del paquete

## Comandos de las ramas Master y Develop

## Comandos de las ramas Feature

## Comandos de las ramas Release

## Comandos de las ramas Hotfix

## Merges

# REVISIONES CRUZADAS

## ¿Qué son las revisiones cruzadas?

## Proceso

## Establece tus reglas

## Herramientas
